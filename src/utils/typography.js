import Typography from "typography"
import elkGlenTheme from "typography-theme-elk-glen"

elkGlenTheme.overrideThemeStyles = ({ rhythm }, options) => ({
  a: {
    textShadow: `none`,
    transition: `0.2s all`,
  },
  p: {
    marginBottom: rhythm(1 / 2),
  },
  "h2,h3,h4": {
    marginTop: rhythm(1),
    marginBottom: rhythm(1 / 2),
  },
})

const typography = new Typography(elkGlenTheme)

export default typography
