import React from "react"
import styled from "@emotion/styled"
import { StaticQuery, graphql } from "gatsby"

import Link from "../components/link"
import Header from "../components/header"

const Sticky = styled.div`
  position: sticky;
  top: 0;
`

const Navbar = styled.nav`
  font-size: 18px;
  background: ${({ theme }) => theme.primary};

  border: 1px solid rgba(0, 0, 0, 0.2);
  padding: 10px;
`

const BaseNav = styled.ul`
  list-style-type: none;
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  margin: 0;

  &.open {
    display: block;
  }
`

const MainNav = styled(BaseNav)`
  display: none;

  @media (min-width: 768px) {
    display: flex;
  }
`

const SubNav = styled(BaseNav)`
  background: ${({ theme }) => theme.inverse};
  border: 1px solid ${({ theme }) => theme.primary};

  a {
    color: ${({ theme }) => theme.primary};
  }

  @media (min-width: 768px) {
    display: none;
    width: inherit;
    position: absolute;
    padding: 10px;
    li {
      padding: 5px 10px;
    }
  }
`

const NavItem = styled.li`
  text-align: center;
  margin: 0;
`

const NavLink = styled(Link)`
  text-decoration: none;
  background-image: none;
  padding: 10px;
  display: block;
  color: ${({ theme }) => theme.inverse};
`

const MobileNav = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;

  @media (min-width: 768px) {
    display: none;
  }
`

const MobileLogo = styled(Link)`
  display: inline-block;
  text-decoration: none;
  font-size: 22px;
  color: ${({ theme }) => theme.inverse};
`

const NavbarToggle = styled.button`
  cursor: pointer;
  background: none;
  border: none;
  text-align: center;
  font-size: 24px;
`

class Navigation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showMenu: false,
    }
  }

  handleClick = e => {
    e.preventDefault()
    this.setState(prevState => ({
      showMenu: !prevState.showMenu,
    }))
  }

  render() {
    return (
      <StaticQuery
        query={graphql`
          query SiteTitleQuery {
            site {
              siteMetadata {
                title
                menuLinks {
                  name
                  link
                  subLinks {
                    name
                    link
                  }
                }
              }
            }
          }
        `}
        render={data => (
          <>
            <Header title={data.site.siteMetadata.title} />
            <Sticky>
              <Navbar>
                <MobileNav>
                  <MobileLogo to="/">{data.site.siteMetadata.title}</MobileLogo>
                  <NavbarToggle
                    onClick={this.handleClick}
                    aria-label="Toggle mobile navigation"
                  >
                    {this.state.showMenu ? `×` : "☰"}
                  </NavbarToggle>
                </MobileNav>
                <MainNav className={this.state.showMenu && "open"}>
                  {data.site.siteMetadata.menuLinks.map((link, index) => {
                    return link.subLinks ? (
                      <Submenu key={index} data={link} />
                    ) : (
                      <NavItem key={index}>
                        <NavLink to={link.link} activeClassName="active">
                          {link.name}
                        </NavLink>
                      </NavItem>
                    )
                  })}
                </MainNav>
              </Navbar>
            </Sticky>
          </>
        )}
      />
    )
  }
}

class Submenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showMenu: true,
    }
  }

  componentDidMount() {
    this.setState({ showMenu: false }) // needed for no-js users
  }

  handleHover = () => {
    this.setState({ showMenu: true })
  }

  handleLeave = () => {
    this.setState({ showMenu: false })
  }
  render() {
    const data = this.props.data
    return (
      <NavItem onMouseLeave={this.handleLeave} onBlur={this.handleLeave}>
        <NavLink
          to={data.link}
          activeClassName="active"
          onMouseEnter={this.handleHover}
          onFocus={this.handleHover}
        >
          {data.name}
        </NavLink>
        <SubNav className={this.state.showMenu && "open"}>
          {data.subLinks.map((link, index) => {
            return (
              <NavItem key={index}>
                <NavLink
                  to={link.link}
                  activeClassName="active"
                  onFocus={this.handleHover}
                  onBlur={this.handleLeave}
                >
                  {link.name}
                </NavLink>
              </NavItem>
            )
          })}
        </SubNav>
      </NavItem>
    )
  }
}

export default Navigation
