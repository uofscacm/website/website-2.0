import React from "react"
import styled from "@emotion/styled"
import { Link as GatsbyLink } from "gatsby"

export const Link = ({
  children,
  to,
  activeClassName,
  partiallyActive,
  ...other
}) => {
  const internal = /^\/(?!\/)/.test(to)
  const file = /\.[0-9a-z]+$/i.test(to)

  if (internal) {
    if (file) {
      return (
        <a href={to} {...other}>
          {children}
        </a>
      )
    }
    return (
      <GatsbyLink
        to={to}
        activeClassName={activeClassName}
        partiallyActive={partiallyActive}
        {...other}
      >
        {children}
      </GatsbyLink>
    )
  }
  return (
    <a href={to} {...other}>
      {children}
    </a>
  )
}

export const StyledLink = styled(Link)`
  background-image: none;

  &.active {
    color: ${({ theme }) => theme.accent};
    font-weight: bold;
    background-image: none;
    transition: 0.2s all;
  }

  &.active:hover {
    background-image: linear-gradient(
      to top,
      rgba(0, 0, 0, 0),
      rgba(0, 0, 0, 0) 1px,
      ${({ theme }) => theme.accent} 1px,
      ${({ theme }) => theme.accent} 2px,
      rgba(0, 0, 0, 0) 2px
    );
  }

  :hover {
    background-image: linear-gradient(
      to top,
      rgba(0, 0, 0, 0),
      rgba(0, 0, 0, 0) 1px,
      ${({ theme }) => theme.primary} 1px,
      ${({ theme }) => theme.primary} 2px,
      rgba(0, 0, 0, 0) 2px
    );
  }
`
export default Link
