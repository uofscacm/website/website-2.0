import React from "react"
import styled from "@emotion/styled"

const Background = styled.div`
  min-height: 300px;

  background: ${props => props.background || "default"};

  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`

const Details = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  justify-content: space-around;
  min-height: 200px;
  padding: 10px;

  h2 {
    margin: 0;
    font-size: 3rem;
    color: white;
  }

  p {
    margin: 0;
    padding-top: 0;
    color: white;
  }
`

const Hero = ({ color, title, date, time, location, author }) => {
  return (
    <Background background={color}>
      <Details>
        <h2 dangerouslySetInnerHTML={{ __html: title }} />
        <div>
          <p>
            <em>
              <time>
                {date && <>{date}</>}
                {time && <>, {time}</>}
              </time>
              {location && <> &mdash; {location}</>}
            </em>
          </p>
          {author && <p>{author}</p>}
        </div>
      </Details>
    </Background>
  )
}

export const heroOptions = {
  bluePurple: "linear-gradient(to right, #4568dc, #b06ab3)",
  greenYellow: "linear-gradient(to right, #43c6ac, #f8ffae)",
  redBlue: "linear-gradient(to right, #b92b27, #1565c0)",
  greyBlue:
    "linear-gradient( 109.6deg,  rgba(0,0,0,1) 11.2%, rgba(11,132,145,1) 91.1% )",
  pinkBlue: "linear-gradient(315deg, #726cf8 0%, #e975a8 74%)",
  yellowBlue: "linear-gradient(90deg, #FDBB2D 0%, #22C1C3 100%)",
  default: "linear-gradient(to right, #ed213a, #93291e)",
}

export default Hero
