export const lightTheme = {
  primary: "#363537",
  inverse: "#FAFAFA",
  accent: "#73000a",
}

export const darkTheme = {
  primary: "#FAFAFA",
  inverse: "#363537",
  accent: "#FAFAFA",
}
