import React from "react"
import { css, Global, withTheme } from "@emotion/react"

const makeGlobalStyles = theme => css`
  body {
    background: ${theme.inverse};
    color: ${theme.primary};
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  a {
    color: ${theme.primary};
  }

  a {
    background-image: linear-gradient(
      to top,
      rgba(0, 0, 0, 0),
      rgba(0, 0, 0, 0) 1px,
      ${theme.primary} 1px,
      ${theme.primary} 2px,
      rgba(0, 0, 0, 0) 2px
    );
  }

  hr {
    background: ${theme.primary};
  }

  blockquote {
    color: ${theme.primary};
    border-color: ${theme.primary};
  }

  .header-links {
    background-image: none;
  }
`

const GlobalStyles = withTheme(({ theme }) => (
  <Global styles={makeGlobalStyles(theme)} />
))

export default GlobalStyles
