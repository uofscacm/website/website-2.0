import React from "react"
import PropTypes from "prop-types"
import styled from "@emotion/styled"

import Navigation from "../components/navigation"
import { ThemeProvider } from "@emotion/react"
import { lightTheme, darkTheme } from "../components/theme"
import GlobalStyles from "../components/global"
import Link from "../components/link"
import Container from "../components/container"

const Footer = styled.footer`
  background-color: black;
  color: white;
  margin: 0;
  a {
    color: white;
    background-image: linear-gradient(
      to top,
      rgba(0, 0, 0, 0),
      rgba(0, 0, 0, 0) 1px,
      #fff 1px,
      #fff 2px,
      rgba(0, 0, 0, 0) 2px
    );
  }

  a:hover {
    background-image: none;
  }
`
class Layout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      darkMode: false,
    }
  }

  setLightMode = () => {
    this.setState({ darkMode: false })
  }

  setDarkMode = () => {
    this.setState({ darkMode: true })
  }

  componentDidMount() {
    this.setState({
      darkMode: window.matchMedia("(prefers-color-scheme: dark)").matches,
    })

    window
      .matchMedia("(prefers-color-scheme: dark)")
      .addListener(e => e.matches && this.setDarkMode())
    window
      .matchMedia("(prefers-color-scheme: light)")
      .addListener(e => e.matches && this.setLightMode())
    window
      .matchMedia("(prefers-color-scheme: no-preference)")
      .addListener(e => e.matches && this.setLightMode())
  }

  render() {
    return (
      <ThemeProvider theme={this.state.darkMode ? darkTheme : lightTheme}>
        <GlobalStyles />
        <Navigation />
        <main id="main">{this.props.children}</main>

        <Footer>
          <Container>
            <div>
              © {new Date().getFullYear()}, Built with{" "}
              <Link to="https://www.gatsbyjs.org">Gatsby</Link> | Site design by{" "}
              <Link to="https://daltoncraven.me">Dalton Craven</Link>
            </div>
            <div>
              View code on{" "}
              <Link to="https://gitlab.com/uofscacm/website/website-2.0">
                GitLab
              </Link>{" "}
              | View our <Link to="/conduct">Code of Conduct</Link>{" "}
              | Email <Link to="mailto:uofscacm@gmail.com">Webmaster</Link>
            </div>
          </Container>
        </Footer>
      </ThemeProvider>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
