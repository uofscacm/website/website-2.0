import styled from "@emotion/styled"

const Container = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 1rem;
`

export default Container
