---
title: End of Semester and Potential Work
date: 2022-04-21
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Lex Whalen
summary: Past and Present Execs and their Experiences.
---

This week we will relax with a destress meeting and hear from Nathan Dolbir at the AI Institute. This summer they will have some positions open, so be sure to check it out if interested.

Furthermore, keep in mind the election for next year's ACM positions. Attached is the link.

[Election Link](https://forms.microsoft.com/Pages/ResponsePage.aspx?id=GUsqSzXRDkKLsrHNI4mYzO_vIbZ9EoVHnrTmxg7igEVURDRMNFE0UTZBWUNEWFZZN0MwSDhGMlZETS4u)

Great job this semester, and finish strong!

---