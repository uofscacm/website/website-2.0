---
title: Talk by Drew Varner
date: 2022-04-07
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Lex Whalen
summary: "NineFx and Company Choice of Programming Language."
---

This Thursday Drew Varner from NineFx will be giving a talk on what it is the company does and how they do it. Drew will explain why certain programming languages were chosen for production and others were not, and what criteria this choice is based on. You can find more about NineFx by visiting their website.

[NineFx](http://www.ninefx.com)

---