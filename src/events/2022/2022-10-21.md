---
title: Fall 2022 Codeathon
date: 2022-10-21
time: 7:00 PM
location: Wired Cafe / Discord
author: Georphoe Lin
summary: "Solve coding problems! Get extra credit! Great interview prep! Win prizes!"
---
## Code-A-Thon: 21 October 2022 - 22 October 2022

We will be offering a hybrid event this year, both on Discord and in person. If you feel comfortable attending in person, we have a spot in the Wired Cafe from 7:00 to 10:00 PM on the 21th. For those who would like to attend via Discord, see the link below. Note that we only are able to set up in the Wired Cafe on Friday and that if you would like to continue on Saturday this must be done via Discord.

Discord: http://discord.gg/uRRAmYD 

GroupMe (Our discord is linked to the groupme: https://groupme.com/join_group/25721586/R5AtRy)

We will be using the Discord voice channels to hang out / chat with other competitors.

## How It Works

When the contest starts at 7 pm on October 21th, you will have a 24 hour time window to submit your answers to algorithm based questions. Think of it as an exam that opens at 7 pm October 21th, but is not due until 7 pm the next day. 

Different questions are worth different amounts of points, depending on their difficulty. For each question, there are test cases that test your solution against a variety of inputs. The percentage of test cases that you get correct determines how many points you get for that problem. (i.e. if a problem is worth 20 points, and your program works on half the test cases, you would get 10 points). 

Each problem statement will have a section called "Constraints". This means the problem writer has guaranteed that the test case input will only be in the format / number range specified. (i.e. There is no point in validating the constraints in your program. If you actually tried to account for every way the input could be messed up, you would spend the entire contest on that instead of the algorithm part :) )

When reading input for the problem, just read in the input as if someone was typing in the console of your IDE (likely the same way you have read input for most of your programs, aka standard input). 

## Prizes

Those that place first in a division will recieve a $100 gift card. Second place winners will recieve a $75 gift card. Third place winners recieve a $50 gift card. Those that place top 3 in each division will be posted on our website (full leaderboard will also be available). You are only eligible for prizes if you are competing in the correct division (or a higher one, limit of 1 prize per person). i.e. If you are in CSCE350 and try competing in the 146 division, you will not be eligible for prizes in the 146 division. Note: Prizes not claimed within 1 month are forfeited automatically and are given back to the ACM club.

## Ethics

It has always been open internet, and mainly an exercise of problem solving skills, but like other years we discourage sharing answers and solutions, instead we emphasize idea sharing and a socially positive environment for everyone. 

## Extra Credit

If you attend in person, there will be a sign in sheet.

Due to the inability to have a sign in sheet for Discord, there will also be a channel where you can post your name, USC email, Hackerank username, class and class section. (We will also post it in our groupme.) This way it is very easy to compile everyone and send a list of participants to professors who are offering extra credit.

## Links to Contest

The division you compete in is based on which of the following courses you are currently in (or last completed if not in any of the following). For example, if you are currently in 145, or have taken it, but are not in 146, and have not taken 146 yet, 145 is the division you may compete within for prizes. If you have yet to take 145 or are not a CSCE major, feel free to jump in for 145.

i.e. If in CSCE240 currently, go to the 240 division. If you finished CSCE240, but have not taken CSCE350, go to the 240 division

### Join This Semester Here:

- [CSCE145 Division](https://www.hackerrank.com/acmusc-codeathon-fall-2022-145)
- [CSCE146 Division](https://www.hackerrank.com/acmusc-codeathon-fall-2022-146)
- [CSCE240 Division](https://www.hackerrank.com/acmusc-codeathon-fall-2022-240)
- [CSCE350 Division](https://www.hackerrank.com/acmusc-codeathon-fall-2022-350)

- [Alumni/Non Students](https://www.hackerrank.com/acmusc-codeathon-fall-2022-alumni) (No prizes, sorry!)

### Tiebreaks

Tiebreaks will be decided as the Hackerank default, as in the first to reach the score will be given precedence.

## Previous Year
Here are the contests for Spring 2022 if you want to practice:
- [CSCE145 Division](https://www.hackerrank.com/acmusc-codeathon-spring-2022-145)
- [CSCE146 Division](https://www.hackerrank.com/acmusc-codeathon-spring-2022-146)
- [CSCE240 Division](https://www.hackerrank.com/acmusc-codeathon-spring-2022-240)
- [CSCE350 Division](https://www.hackerrank.com/acmusc-codeathon-spring-2022-350)

- [Alumni/Non Students](https://www.hackerrank.com/acmusc-codeathon-spring-2022-alumni) (No prizes, sorry!)

Here are the solutions for the Spring 2022 Problems:
- [Last year lower Division Solutions](https://github.com/j-blake-s/Code-A-Thon-S22-Lower)
- [Last year upper Division Solutions](https://github.com/j-blake-s/Code-A-Thon-S22-Upper)

---
