---
title: Winter Social
date: 2022-11-17
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary: Last Meeting of the Semester
---

The time has come at last... Tonight will be the last meeting of the semester for ACM 😭. We will be meeting at 7:00 pm in our standard meeting room SWGN 2A17 for pizza and dessert for our winter social. There will NOT be a presentation after the meeting to let everyone relax and prepare for the break and exams. See you then!


---
