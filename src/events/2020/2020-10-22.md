---
title: Virtual Ice Cream Social
date: 2020-10-22
time: 7:00 PM
location: Discord
summary: "Bring ice cream."
---
We will have a virtual ice cream social tonight at 7 pm. Bring ice cream and join us on the Discord. We have some eventful talks coming up, including a talk on caching that will be given by James Coman, a USC alumni and current graduate student at Texas A&M. Next week we have talk proposals ranging from “Evil  Corners of C”, “Lambdas and Function Programming”, and “Spooky Hacker Stories”, that are TBD.
