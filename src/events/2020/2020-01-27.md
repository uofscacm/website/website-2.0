---
title: Volunteering at Oak Pointe
date: 2020-01-27
time: 4:00 PM
location: Riverbottom Road, Irmo, SC 29063
author: Joshua Nelson
summary: ACM will be volunteering at STEM Night at a local elementary school!
---

Instead of our normal talks, ACM's meeting this week will be volunteering at Oak Pointe Elementary School for STEM night.
Feel free to join us - if you need a ride, you can reach out to us on GroupMe or via email (linked above).
