---
title: Getting Started in Open Source
date: 2020-02-13
time: 7:00 PM
location: Swearingen Engineering Center, room 2A17
author: Joshua Nelson
summary: "How to contribute to open source: finding a project, deciding what to work on, communicating with the maintainers, and making a PR."
---


This week's talk will be on how to contribute to open source:

- finding a project
- deciding what to work on
- communicating with the maintainers
- making your first pull request (PR)

I'll use real PR's I've made to projects as examples of what and what not to do. There will be a discussion of how to use common open-source tools like git, markdown, continuous integration (CI) providers, and unit tests as they pertain to contributing to a project.

If time permits, I can help attendees find projects they'd be interested in contributing to.

Note: This talk was originally planned for the 6th, but was postponed due to bad weather.

## Links

The slides for this talk are [available here](/assets/2020-02-13/).
