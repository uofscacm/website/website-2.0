---
title: Welcome All!
date: 2023-08-31
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary: Welcome to the first meeting of the semester!
---

Get ready for our informative interest meeting where we'll dive into what our club stands for, how our meetings operate, and the exciting plans we've lined up for the semester. Discover our club's mission, meeting structure, and get a sneak peek of upcoming presentations and events. Don't miss out—whether you're a veteran member or a newcomer, this meeting sets the stage for an enriching semester filled with camaraderie and learning. Come hangout with us, enjoy pizza and join our Discord!

---
