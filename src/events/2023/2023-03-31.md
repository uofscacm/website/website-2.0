---
title: ACM Tech Connect
date: 2023-03-31
time: 1:00 PM
location: Swearingen Engineering Amoco Hall 1C01
author: Georphoe Lin
summary: ACM Tech Connect
---
No meeting Thursday, please come out Friday :)
Also start thinking about joining the exec team for next year. Nominations will be going out soon. Thanks!

---
