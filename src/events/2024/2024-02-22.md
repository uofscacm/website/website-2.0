---
title: Embark on Your DevOps Journey
date: 2024-02-22
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary: Embark on Your DevOps Journey -- An Introductory Meeting with GitHub
---

Good morning! The topic for tonight’s meeting is Intro to DevOps. DevOps is a highly sought-after developer position that oversees the entire development lifecycle. Learn how to get started in DevOps with GitHub tonight!

We will have pizza and drinks in SWGN 2A17 at 7:00 pm. The presentation will start at 7:30 pm. See you then!
---
