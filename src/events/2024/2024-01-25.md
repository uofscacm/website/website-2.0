---
title: Semester Social -- Game Night Extravaganza!
date: 2024-01-25
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary: Join us tonight at 7:00 pm in SWGN 2A17 for an exciting game night featuring Jackbox games.
---

Our third meeting of the semester is tonight at 7:00 pm in SWGN 2A17. We will have a game night tonight featuring Jackbox games. Feel free to bring any additional games you want to play! We will have pizza, drinks, and dessert as always 🙂

---
