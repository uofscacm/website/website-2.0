---
title: Pizza and Coding Night -- Building a Modern Website with NextJS
date: 2024-01-18
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary:  Tonight, we'll dive into the world of web development by building a modern and responsive website using NextJS.
---

Join us for pizza, drinks, and dessert tonight at 7:00 pm in SWGN 2A17. We’ll be building a modern, responsive website using NextJS starting at 7:30 pm. 
 
Basic programming understanding is recommended, but experience with NextJS or JavaScript is not required. See you tonight!


---
