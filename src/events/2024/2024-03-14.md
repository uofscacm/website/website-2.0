---
title: Harnessing Retrieval Augmented Generation for Reliable Data Conversations
date: 2024-03-14
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary: A meeting focused on Retrieval Augmented Generation with LLMs
---

Good morning! The topic for tonight’s meeting is Retrieval Augmented Generation with LLMs (have conversations with your data). This allows you to ensure factual information is provided and the LLM can reference where it got its information from, which gets around the problem of LLM hallucinations. We'll go over how simple it is to set up and how you could use it and how you could sell it to a company since so many want it.

We will have pizza and drinks in SWGN 2A17 at 7:00 pm. The presentation will start at 7:30 pm. See you then!
---
