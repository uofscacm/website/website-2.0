---
title: Movie Night Extravaganza!
date: 2024-02-15
time: 7:00 PM
location: Swearingen Engineering Center, 2A17
author: Georphoe Lin
summary: Movie Night Extravaganza -- A Break from Studies and a Feast for the Senses!
---

Time for another meeting! We will meet tonight at 7:00 pm in SWGN 2A17 for pizza, drinks, and dessert. Tonight is movie night! Take a break from exams and studying and relax with a great movie. We will vote on the movie to watch at 7:00 based on the suggestions in the survey and start watching after that. See you then!

---
