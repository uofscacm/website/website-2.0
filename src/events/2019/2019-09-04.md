---
title: Basics of Web Development
date: 2019-09-02
time: 7:00 PM
location: Swearingen 2A19
author: Dalton Craven
tags:
  - "Talk"
summary: Dalton Craven will be going over the basics of web development. We'll be working with HTML, CSS, and JavaScript, and will finish the talk by creating a personalized website! No prior web dev experience is needed, but beginners and experts alike are welcome to join.
---

ACM is having its second talk of the semester on how to build a website.
Dalton Craven will be going over the basics of web development.
We'll be working with HTML, CSS, and JavaScript, and will finish the talk by creating a personalized website!
No prior web dev experience is needed, but beginners and experts alike are welcome to join.

We look forward to seeing you!

---

UPDATE: Here are [the slides from the talk](https://drive.google.com/open?id=1JYcc6Wrqwta5Fvz-taPTVGLtk2Mv6xAmWaD18xCvkyM).
