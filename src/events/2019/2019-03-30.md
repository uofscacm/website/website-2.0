---
title: Fix-It-Day
date: 2019-03-30
time: 10:00 AM
location: 300 Main
author: Brady O'Leary
tags:
  - "CommunityService"
  - "WeNeedVolunteers"
summary: Fix-it day is our annual Community Service Event where we fix people's computers for free. We need volunteers.
---

## Information for the General Public

Is your computer or laptop running slow, getting too hot, or riddled with viruses? Students in the USC College of Engineering's Association for Computing Machinery are hosting its annual Fix-It Day on March 30, 2019!

On Fix-It Day, student ACM members will repair your laptop or desktop for free, (but you are welcome to make a donation)! You do not have to be affiliated with the university to participate.

Computers will be fixed on a first-come first-served basis from 10 a.m. to 3 p.m. If your computer cannot be fixed, the students will recycle it for you, free of charge.

Here's a quick note on what to expect:
We cannot fix broken hardware such as screens or other damaged components. If you would like a replacement part installed, you need to bring it with you.

Examples of tasks that we can do:

- Virus removal

- password reset

- Installation or uninstallation of programs (if you would like a program
  installed, please bring any software disks and license keys)

- PC tune-up

- Full PC diagnosis

- Simple data recovery

- Some mechanical problems, such as loose hinges, as long as replacement parts are not required

- OS installation (please bring disks, recovery media, and license keys)

PLEASE back up all data before you come. We are not responsible for damage or loss of data or hardware of any kind. You must sign a waiver before your computer will be worked on.

[Facebook Link](https://www.facebook.com/events/367877034044272/)

---

## Volunteer Info

If you are interested in volunteering, please send an email to [uofscacm@gmail.com](mailto:uofscacm@gmail.com). We will be having a training session On Wednesday, March 27th, 2019, after the regular ACM Meeting. If you cannot make this, it's not a big deal, and we can work around that.

We need volunteers to help us with fixing computers at this event. We are asking that people try to be there at 9:00am to help set up. **Lunch will be provided for all volunteers.**
