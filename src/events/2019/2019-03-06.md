---
title: Open Source South Carolina
date: 2019-03-06
time: 6:30 PM
location: SOCO Vista
author: Brady O'Leary
tags:
  - "OpenSourceSouthCarolina"
  - "WebApps"
  - "#ThereWillBePizzaToo!"
  - "🍕🍕🍕🍕🍕"
summary: Our normal meeting this week will be held at Open Source South Carolina in the Vista. Read on for more details.
---

**Location:** SOCO Vista (above Jason's Deli).

823 Gervais St. \#220

Columbia, SC

&nbsp;

**Please Note:** The meeting begins at 6:30pm, instead of the usual 7:00pm.

This week, we are going to Open Source South Carolina for our meeting.

[Click Here for More Info](https://www.meetup.com/Open-Source-South-Carolina/events/258711877/)
