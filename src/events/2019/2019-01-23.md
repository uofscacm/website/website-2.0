---
title: Introduction to Python Talk
date: 2019-01-23
time: 7:00 PM
location: Swearingen 2A27
author: Joshua Nelson
tags:
  - "Talk"
  - "#ThereWillBePizzaToo!"
  - "🍕🍕🍕🍕🍕"
summary: Our fist meeting will be on Wednesday, <b>January 23rd, 2019</b> in <b> Swearingen 2A27</b> and will be an introduction to the Python programming language.
---

ACM is having its first talk of the 2019 Spring semester, and the topic is Python!
The talk will be on January 23rd, in Swearingen 2A27, at 7:00 P.M.
Joshua Nelson will be hosting the talk.
It will cover introductory Python from the perspective of a sophomore who has just finished 146.
Prior programming experience is recommended but not required.

The following subjects will be covered, as well as many others:

- file IO
- string manipulation
- syntax differences from C and Java
- what makes python easy to read and write
- what makes code 'pythonic'
- classes and operator overloading
- various common and useful libraries

We look forward to seeing you!

---

Update: [the slides are here](/presentations/2019-01-23/index.html)!
