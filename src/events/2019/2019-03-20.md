---
title: A Guide to Being AWKward
date: 2019-03-20
time: 7:00 PM
location: Swearingen 2A19
author: Charles Daniels
tags:
  - "AWK"
  - "#ThereWillBePizzaToo!"
  - "🍕🍕🍕🍕🍕"
summary: Charles Daniels will be giving a talk on AWK, a simple programming language for manipulating streams of text.
---

For our weekly ACM talk, Charles Daniels will be giving a talk on AWK. Awk is a simple programming language for manipulating streams of text. It is very versatile and useful for tasks such as searching or processing text, interacting with tabular data (such as CSV, TSV), and rapidly prototyping algorithms or ideas. Awk has been a mainstay UNIX tool for decades due to its simplicity and speed. In this talk, I will introduce the basic principles and usage of Awk, show some common idiomatic examples of usage in depth, and discuss and demonstrate more advanced usage in brief.

---

We also have a few other exciting things coming up:

1. There will be a New CEC Mentor Program starting in the Fall! This will pair upperclassmen to new students. **Note that the deadline is Friday, March 22<sup>nd</sup> 2019, or the Friday AFTER this meeting.** If you are interested, read more about it and complete the application at [https://sc.edu/study/colleges_schools/engineering_and_computing/my_cec/mentoring/index.php](https://sc.edu/study/colleges_schools/engineering_and_computing/my_cec/mentoring/index.php).

2. The Industrial Advisory Board Meeting is going to be on April 5th. Please look for another entry on the Home page for this.

3. We need volunteers for Fix-it Day. This is our yearly community service event where we fix people's computers for free. If you are interested, please let one of the officers know. More information will be made available soon.

---

### Downloads

[Link to Talk Resources](http://cdaniels.net/talks.html#A%20Guide%20To%20Being%20AWKWard)
