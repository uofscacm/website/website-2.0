---
title: Monogame
date: 2019-02-27
time: 7:00 PM
location: Swearingen 2A19
author: Colter Boudinot
tags:
  - "GameDev"
  - "#ThereWillBePizzaToo!"
  - "🍕🍕🍕🍕🍕"
summary: Colter will be giving a talk on Making Videogames using Monogame, and using C#.
---

This week Colter Boudinot will be giving a talk on Developing Games with C# and Monogame.

&nbsp;

> In this talk, I will show the basics of how to create a game using C# with the Monogame framework. Monogame allows for easier creation of games compared to just using C# by itself. The syntax for C# is very similar to Java, with very few differences. If you have experience with Java, you'll already know the basics of C#. Great for if you want to get into game development. If you want follow along, you can download visual studio (the IDE) and MonoGame (the framework that allows for game creation).

Here are links for Microsoft VisualStudio and Monogame.

---

### Downloads

[VisualStudio](https://visualstudio.microsoft.com/downloads/)

[Monogame](http://www.monogame.net/downloads/)
