---
title: Functional Languages
date: 2019-09-25
time: 7:00 PM
location: Swearingen 2A17
author: Justin Baum
tags:
  - "Talk"
summary: An in-depth presentation going through why you should use a functional language to make yourself a better programmer.
---

Join us tomorrow night at 7pm in Swearingen 2A17 for free pizza followed by talk by Justin Baum at 7:15 PM on functional languages.

Justin describes the talk:

> We will be doing a talk about what functional languages are. Specifically why referential transparency, immutability, and pure languages are so important, and what they mean. It will be a in-depth presentation going through why you should use a functional language to make yourself a better programmer.

This talk is a bit more advanced than some in our past, but I would imagine there's something of interest for everyone. There's value in being exposed to functional programming even if you never become an expert in it.

We hope to see you there!
