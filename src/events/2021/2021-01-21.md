---
title: Timeline of an Undergraduate Degree
date: 2021-01-21
time: 7:00 PM
location: Discord
author: Panel Discussion
summary: "A panel discussion that will answer questions, and offer advice for various tracks. Please bring questions, and all questions and interjections are welcome."
---

Considering how important this talk can be, we’ve decided to change the talk to a panel discussion. We will have a panel of alumni, graduate students, and seniors to answer questions and guide through a rough outline of advice. We are inviting everyone to bring questions, anecdotes, experiences and advice to our discussion this Thursday night. Everyone is welcome to ask questions about how to best achieve your goals academically and professionally. We originally were planning to discuss things worth learning in and outside of the classroom, but after some discussion it seems that this form may be closely tied to certain tracks, and everyones’ goals are varied. We can discuss electives, tools and languages, or tips for job interviews.

[](Add participants)
