---
title: Lunch with Cognito Forms
date: 2021-11-18
time: 12:00 PM
location: 2277 Horizon II
author: Lex Whalen
summary: "Lunch with a local business."
---

This week ACM will be meeting with local business Cognito Forms for some networking and lunch. Cognito forms is centered in Columbia, and is a quickly growing business. They specialize in form design, from contact to registration, and the user-friendliness they provide in form customization is a point of focus.

Join us at Horizon II to have lunch and the opportunity to speak with a business nearby.

---
### More on Cognito Forms:
- https://www.cognitoforms.com/