---
title: Computability
date: 2021-10-28
time: 7:00 PM
location: Swearingen Engineering Center
author: Lex Whalen
summary: "A brief intro to Computability."
---

This week former U of SC student Justin Baum will be talking about computability. The talk will involve an overview of the subject, and lead up to undecidability and NP-Complete. It will also touch on different types of computation methods, such as mechanical or DNA.


