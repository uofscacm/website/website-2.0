---
title: Dessert Social w/ WiC and MiC!
date: 2021-09-09
time: 7:00 PM
location: Swearingen Engineering Center
author: Lex Whalen
summary: "Have fun with members of similar clubs!"
---

This Thursday we will be having a dessert social with Women in Computing and Minorities in Computing. Look out for ice cream or cookies!

### Find more on both clubs on Garnet Gate:

- [WiC](https://garnetgate.sa.sc.edu/organization/wic)
- [MiC](https://garnetgate.sa.sc.edu/organization/mic)

---