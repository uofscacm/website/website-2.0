---
title: Talk by Thomas from Garnet Game Jammers
date: 2021-10-01
time: 7:00 PM
location: Swearingen Engineering Center
author: Lex Whalen
summary: "A Talk by Thomas from GGJ."
---

This week Thomas from Garnet Game Jammers (GGJ) will give a talk. GGJ is a place for those interested in game development to share ideas and become better at making games.

Be sure to check them out on Garnet Gate!

- [GGJ](https://garnetgate.sa.sc.edu/organization/ggj)

---