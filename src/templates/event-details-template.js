import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Hero, { heroOptions } from "../components/hero"
import SEO from "../components/seo"
import Container from "../components/container"
import { defineCustomElements as deckDeckGoHighlightElement } from "@deckdeckgo/highlight-code/dist/loader"

const EventDetails = ({ data }) => {
  const post = data.markdownRemark
  deckDeckGoHighlightElement()
  return (
    <Layout>
      <SEO title={post.frontmatter.title.replace(/<[^>]+>/g, "")} />
      <Hero
        color={heroOptions.bluePurple}
        title={post.frontmatter.title}
        date={post.frontmatter.date}
        time={post.frontmatter.time}
        location={post.frontmatter.location}
        author={post.frontmatter.author}
      />
      <Container>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </Container>
    </Layout>
  )
}

export default EventDetails

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        time
        location
        author
        tags
      }
    }
  }
`
