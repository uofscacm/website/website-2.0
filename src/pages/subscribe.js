import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero, { heroOptions } from "../components/hero"
import Container from "../components/container"
import Link from "../components/link"

const AboutPage = () => (
  <Layout>
    <SEO title="Email List" />
    <Hero title="ACM@USC Email List" color={heroOptions.yellowBlue} />
    <Container>
      <p>
        The best way to keep up with ACM@USC events is to subscribe to our
        official UofSC ListServ. This allows you to receive regular updates
        about upcoming events and opportunities without having to constantly
        check the website. We do not send out spam.
      </p>
      <h2>How do I Subscribe?</h2>
      <hr />
      <p>
        The easiest way to subscribe to our list is to simply come to the next
        meeting. We automatically add new people to the mailing list with the
        given email on the sign-in sheet.
      </p>
      <p>
        Additionally, you can subscribe using the{" "}
        <Link to="http://listserv.sc.edu/cgi-bin/wa?SUBED1=UOFSCACM&A=1">
          online subscription form
        </Link>
        . Here, you can either fill out your name/email or log into your
        university account.
      </p>
      <h2>What's in the Emails?</h2>
      <hr />
      <p>
        These emails contain updates and information on the happenings of the
        club, focusing on nearby events. We generally send out emails once or
        twice a week to remind our members of events happening with the club, as
        well as external opportunities available.
      </p>
      <h2>How do I Stop Receiving Emails?</h2>
      <hr />
      <p>
        We include an unsubscribe link in the bottom of our emails. You can also
        go to listserv.sc.edu, login, and unsubscribe from our mailing list.
      </p>
    </Container>
  </Layout>
)

export default AboutPage
