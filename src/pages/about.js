import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero, { heroOptions } from "../components/hero"
import Container from "../components/container"
import Link from "../components/link"

const AboutPage = () => (
  <Layout>
    <SEO title="About Us" />
    <Hero title="About ACM@USC" color={heroOptions.redBlue} />
    <Container>
      <h2>Who We Are</h2>
      <hr />
      <p>
        We are a Student Organization that regularly meets to share our interest
        in various subjects in computing. We welcome people of all majors and
        backgrounds to come out and learn more about various topics in
        computing. Many of our talks tend to focus more on various types of
        Software and Programming Languages, but occasionally we have talks on
        hardware and other topics as well. We are certainly open to students
        wanting to give talks.
      </p>
      <h2>When we Meet</h2>
      <hr />
      <p>
        We hold regular meetings throughout the semester on Wednesdays at 7:00pm
        in the ACM Room (Swearingen 2A17) where there is usually pizza. These
        meetings usually consist of one of our students giving a talk on a great
        variety of computing-related subjects. You can find more information
        about what these meetings are about by checking our home page, as we
        will post on upcoming meeting topics.
      </p>
      <h2>Events</h2>
      <hr />
      <p>We hold a few special events throughout each semester.</p>
      <h3>Code-a-thon</h3>
      <p>
        The Code-a-thon is a 24-hour event where students compete to see who can
        solve the most coding problems the fastest, with prizes being given to
        the top 3 students in each division. There are 2 divisions, of which are
        split into 2 sub-categories depending on which CSCE class was last taken
        (out of CSCE145, CSCE146, CSCE240, and CSCE350. Students who have never
        taken any of these classes will fall into the CSCE145 category) in order
        to keep the content more fair to less experienced students. The
        competition is held in the Swearingen Windows Computer Labs on the 1st
        floor of Swearingen in D-Wing, however, since the competition is held
        online, students are not actually required to attend in person, and may
        compete from wherever they choose. You will hear more about this as we
        get close to the event.
      </p>
      <h3>Special Interest Meetings</h3>
      <p>
        Occasionally a professor or person involved in the Industry will give a
        talk on a topic. Typically, these may happen once or twice a semester,
        and will usually occur on a different day than the normal meetings.
      </p>
      <h3>Hack-a-thon</h3>
      <p>
        In the Spring of 2018, the Computer Science Department, with help from
        our student chapter and a few other Computing Student Organizations, put
        on ColaHacks, an event where students from a multitude of Colleges and
        Universities had 12 hours to completely write a piece of software that
        would help the people of Columbia, SC. Check the home page for more info
        during the Spring Semester for more information about this event.
      </p>
      <h2>Community Service</h2>
      <hr />
      <p>
        In the Spring, we generally hold a Fix-It-Day, where members of this
        organization take a Saturday to fix Computers and also recycle old
        computers for free in order to give back to the Columbia Community.
      </p>
      <h2>Contact Us</h2>
      <hr />
      <p>
        If you are interested in learning more about our ACM Chapter, or are
        interested in sponsoring an event, we can be reached at the following
        emails:
      </p>
      <p>
        <Link to="mailto:soACM@mailbox.sc.edu">soACM@mailbox.sc.edu</Link>
      </p>
      <p>
        <Link to="mailto:uofscacm@gmail.com">uofscacm@gmail.com</Link>
      </p>
      <p>
        You should hear back in a few days from sending the email, as both are
        regularly checked.
      </p>
    </Container>
  </Layout>
)

export default AboutPage
