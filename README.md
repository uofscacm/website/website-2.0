# ACM@USC Website

This is the digital home of [ACM@USC](https://acm.cse.sc.edu), the University of South Carolina's computer science association.

## Installation

To install the required dependencies for developing, use Yarn v1.0 (for now...).

```bash
yarn install
```

## Usage

Use [gatsby-cli](https://www.gatsbyjs.org/docs/gatsby-cli/) to develop or build the website.

```bash
yarn develop
```

```bash
yarn build && yarn serve
```

## Adding a post

Make a new file in `src/events/` named after the date (`YYYY-MM-DD.md`).
You will need the following [YAML](https://yaml.org/) metadata in the header (modify as necessary), 
followed by the body of the post:
```
---
title: "title"
date: "2020-01-01"
time: "7:00 PM"
location: "Swearingen 2A17"
author: "Joshua Nelson"
tags:
 - "#ThereWillBePizza"
summary: "This will be an ACM talk"
---
Hello, world!
```

You can also use the existing posts as an example.
Post bodies are written in [markdown](https://www.markdownguide.org/basic-syntax).

### Static files

If you need to have HTML or binary files that uploaded directly,
you can put the files in `static/` and they will be served from the root directory.
If possible, try to keep the same directory structure:
`presentations/YYYY-MM-DD/your-content` or `events/YYYY-MM-DD/your-content`.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
