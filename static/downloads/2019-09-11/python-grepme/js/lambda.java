import java.util.function.IntConsumer;

class lambda {
  public static void main(String[] args) {
    call_with_one(i -> System.out.println(i));
  }
  static void call_with_one(IntConsumer f) {
    f.accept(1);
  }
}

