// This version only works with a member named `B`
/*
template<typename T> T add(typename T::B a, typename T::B b) {
*/
template<typename T> auto add(T a, T b) -> decltype(a + b) {
    return a + b;
}

// This version works only with strings
void add(const char *a, const char *b) {
    return;
}

int main(void) {
    // Both are valid!
    add("a", "b");
    return add(1, 2);
}
