module.exports = {
  siteMetadata: {
    title: `ACM@USC`,
    description: `The online home of ACM@USC, the University of South Carolina's largest computer science organization.`,
    author: `@cravend123`,
    siteUrl: "https://acm.cse.sc.edu",
    menuLinks: [
      {
        name: "Home",
        link: "/",
      },
      {
        name: "About ACM@USC",
        link: "/about",
        subLinks: [
          {
            name: "Discord",
            link: "http://discord.gg/uRRAmYD",
          },
          {
            name: "GarnetGate",
            link: "https://garnetgate.sa.sc.edu/organization/acm",
          },
          {
            name: "GitLab",
            link: "https://gitlab.com/uofscacm",
          },
          {
            name: "GroupMe",
            link: "https://groupme.com/join_group/25721586/R5AtRy",
          },
          {
            name: "Code of Conduct",
            link: "/conduct",
          },
          {
            name: "Email List",
            link: "/subscribe",
          },
        ],
      },
      {
        name: "About ACM",
        link: "https://www.acm.org/",
      },
      {
        name: "Cool Links",
        link: "/links",
      },
    ],
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/events`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 960,
            },
          },
          {
            resolve: `gatsby-remark-autolink-headers`,
            options: {
              offsetY: 75,
              isIconAfterHeader: true,
              className: "header-links",
            },
          },
          {
            resolve: `gatsby-remark-highlight-code`,
            options: {
              theme: "base16-dark",
            },
          },
        ],
      },
    },
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        createLinkInHead: true,
      },
    },
    `gatsby-redirect-from`,
    `gatsby-plugin-meta-redirect`,
  ],
}
